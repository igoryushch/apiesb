package ua.np.services.api;

import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.junit.Test;

/**
 * Copyright (C) 2014 Nova Poshta. All rights reserved.
 * http://novaposhta.ua/
 * <p/>
 * for internal use only!
 * <p/>
 * User: yushchenko.i
 * email: yushchenko.i@novaposhta.ua
 * Date: 22.05.2014
 */

public class CamelRouteTest extends CamelBlueprintTestSupport{

    // override this method, and return the location of our Blueprint XML file to be used for testing
    @Override
    protected String getBlueprintDescriptor() {
        return "/OSGI-INF/blueprint/blueprint.xml," +
                "/OSGI-INF/blueprint/blueprint-props.xml";
    }

    @Test
    public void testRoute() throws Exception {

        context.getRouteDefinition("httpStreetsDelegate").adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                replaceFromWith( "seda:from" );
            }
        });

        context.getRouteDefinition("sync_route").adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                mockEndpoints( "http*" );
                weaveById( "api2_sync" ).replace().to("mock:api2_sync").stop();
            }
        });

        context.getRouteDefinition("async_processing").adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                mockEndpoints("http*");
                weaveById( "api2_async" ).replace().to("mock:api2_async").stop();
            }
        });

        MockEndpoint api2SyncEndpoint = context().getEndpoint( "mock:api2_sync", MockEndpoint.class );
        MockEndpoint api2AsyncEndpoint = context().getEndpoint( "mock:api2_async", MockEndpoint.class );

        template.setDefaultEndpoint( context().getEndpoint( "seda:from" ) );

        // we must manually start when we are done with all the advice with
        context.start();
        template.sendBody( getMessageBodyForSyncRoute() );
        template.sendBody( getMessageBodyForAsyncRoute() );

        api2SyncEndpoint.expectedMessageCount( 1 );
        api2SyncEndpoint.expectedBodiesReceived( getExpectedMessageBodyForSyncRoute() );
        api2AsyncEndpoint.expectedMessageCount(1);
        api2AsyncEndpoint.expectedBodiesReceived( getExpectedMessageBodyForAsyncRoute() );

        assertMockEndpointsSatisfied();
    }

    private String getMessageBodyForSyncRoute(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<fileGroup>" +
                "<file>" +
                "<apiKey>889ea47fd921890478b7a21a541b5738</apiKey>" +
                "<modelName>Address</modelName>" +
                "<calledMethod>getCities</calledMethod>" +
                "<methodProperties>" +
                "<MainCitiesOnly>true</MainCitiesOnly>" +
                "</methodProperties>" +
                "</file>" +
                "</fileGroup>";
    }

    private String getMessageBodyForAsyncRoute(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<fileGroup>" +
                "<file>" +
                "<apiKey>889ea47fd921890478b7a21a541b5738</apiKey>" +
                "<modelName>Address</modelName>" +
                "<calledMethod>getCities</calledMethod>" +
                "<methodProperties>" +
                "<MainCitiesOnly>true</MainCitiesOnly>" +
                "</methodProperties>" +
                "<callBackUrl>http://someclienthost</callBackUrl>" +
                "</file>" +
                "</fileGroup>";
    }

    private String getExpectedMessageBodyForSyncRoute(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<file>" +
                "<apiKey>889ea47fd921890478b7a21a541b5738</apiKey>" +
                "<modelName>Address</modelName>" +
                "<calledMethod>getCities</calledMethod>" +
                "<methodProperties>" +
                "<MainCitiesOnly>true</MainCitiesOnly>" +
                "</methodProperties>" +
                "</file>";
    }

    private String getExpectedMessageBodyForAsyncRoute(){
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<file>" +
                "<apiKey>889ea47fd921890478b7a21a541b5738</apiKey>" +
                "<modelName>Address</modelName>" +
                "<calledMethod>getCities</calledMethod>" +
                "<methodProperties>" +
                "<MainCitiesOnly>true</MainCitiesOnly>" +
                "</methodProperties>" +
                "<callBackUrl>http://someclienthost</callBackUrl>" +
                "</file>";
    }
}