АПИ 3
======================

#### Запуск SoapUI тестов ####

    > mvn soapui:test

#### Деплой на фабрику ####

    > mvn fabric8:deploy -DskipTests=true
    
#### Деплой на фабрику в fab01.dev.np.ua ####
    
    > mvn fabric8:deploy -DprofileConfigDir=api3/src/main/fabric8/dev -DjolokiaUrl=http://fab01.dev.np.ua:8181/jolokia
        
# Build and install

```
mvn clean install
```

# Provisioning

## Installation and initial configuration

```
Fabric8:karaf@root>
```

## Start Fabric Ensemble
```
fabric:create --clean --wait-for-provisioning
```

## Define our own profile
```
profile-create --parents np_default np_profiles-api3
profile-edit --repositories mvn:ua.np/feature-api3/3.9.0-SNAPSHOT/xml/features np_profiles-api3
profile-edit --features api3 np_profiles-api3
profile-edit --bundles mvn:ua.np/api3/3.9.0-SNAPSHOT np_profiles-api3
```

## Create and run new container with newly created profile

```
container-create-child --profile np_profiles-api3 root dev-api3
```

## Delete container if necessary

```
container-delete test-api3
```