package ua.np.services.api;

import java.util.regex.Pattern;

/**
 * Copyright (C) 2014 Nova Poshta. All rights reserved.
 * http://novaposhta.ua/
 * <p/>
 * for internal use only!
 * <p/>
 * User: yushchenko.i
 * email: yushchenko.i@novaposhta.ua
 * Date: 07.07.2014
 */
public class ExchangeUtils {

    public String removeBulkNodes(String exchangeBody){
        exchangeBody = exchangeBody.replaceAll( Pattern.quote( "<fileGroup>" ), "" );
        return exchangeBody.replaceAll( Pattern.quote( "</fileGroup>" ), "" );
    }

}
